plugins {
    kotlin("jvm") version "2.0.0"
    kotlin("plugin.serialization") version "2.0.0"
    id("app.cash.sqldelight") version "2.0.2"
    application
}

group = "org.mcwarriors.minigames"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
    // maven(url = "https://repo.worldmandia.cc/releases")
}

dependencies {
    implementation("net.minestom:minestom-snapshots:1ef0e26da8")
    implementation("com.github.shynixn.mccoroutine:mccoroutine-minestom-api:2.18.1") // ref: https://github.com/Shynixn/MCCoroutine/pull/121
    implementation("com.github.shynixn.mccoroutine:mccoroutine-minestom-core:2.18.1") // these versions wont work unless you uncomment the worldmani repo
    implementation("org.codeberg.JxBP:ReaKt:v1.0")
    implementation("net.kyori:adventure-text-minimessage:4.17.0")
    implementation("org.slf4j:slf4j-simple:2.0.13")

    implementation("app.cash.sqldelight:sqlite-driver:2.0.2")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.7.0")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(21)
}

application {
    mainClass = "org.mcwarriors.minigames.MainKt"
}

sqldelight {
    databases {
        create("Database") {
            packageName.set("org.mcwarriors.minigames")
        }
    }
}
