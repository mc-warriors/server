package org.mcwarriors.minigames.database

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.jdbc.sqlite.JdbcSqliteDriver
import org.mcwarriors.minigames.Manager
import org.mcwarriors.minigames.data.PlayersQueries
import org.mcwarriors.minigames.file.ResourceManager
import java.io.File
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists

val dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss") // Date format for the database backups - used in file name

object Database {
    val stagedDatabasePath = "${ResourceManager.DIR_DATABASE}/minigames.db"

    var driver: SqlDriver private set
    val players: PlayersQueries

    // Database auto-inits on program start. This could definitely be up for change.
    init {
        val shouldCreateSchema = !Path(stagedDatabasePath).exists()
        driver = JdbcSqliteDriver(
            "jdbc:sqlite:$stagedDatabasePath",
            properties = Properties().apply { put("foreign_keys", "true") })
        if (shouldCreateSchema) clean(false)

        players = PlayersQueries(driver)
    }

    fun clean(delete: Boolean) {
        if(delete) File(stagedDatabasePath).delete()
        org.mcwarriors.minigames.Database.Schema.create(driver)
    }

    fun restore(backupName: String): Boolean {
        driver.close()

        try {
            File("${ResourceManager.DIR_DATABASE}/$backupName").copyTo(File(stagedDatabasePath), overwrite = true)
        } catch(e: NoSuchFileException) {
            Manager.logger.warn("A database restore was called, but the desired backup did not exist. No changes have been made.")
            e.printStackTrace()
            return false
        } catch(e: IOException) {
            Manager.logger.error("A database restore was called, but an IO exception was caught. The database may have corrupted.")
            e.printStackTrace()
            return false
        }


        driver = JdbcSqliteDriver(
            "jdbc:sqlite:$stagedDatabasePath",
            properties = Properties().apply { put("foreign_keys", "true") })

        return true
    }

    fun backup(): String {
        val backupFileName = "${dateFormat.format(LocalDateTime.now())}.db.backup"
        File(stagedDatabasePath).copyTo(File("${ResourceManager.DIR_DATABASE}/$backupFileName"), overwrite = false)

        try {
            File("${ResourceManager.DIR_DATABASE}/$backupFileName").copyTo(File(stagedDatabasePath), overwrite = true)
        } catch(e: NoSuchFileException) {
            Manager.logger.warn("A database backup was called, but the target backup already exists. No changes have been made.")
            e.printStackTrace()
            return ""
        } catch(e: IOException) {
            Manager.logger.warn("A database backup was called, but an IO exception was caught. The original database is unaffected.")
            e.printStackTrace()
            return ""
        }

        return backupFileName
    }
}