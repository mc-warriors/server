package org.mcwarriors.minigames.constants

import net.kyori.adventure.key.Key
import net.kyori.adventure.text.format.TextColor

class Color(private val rgbValue: Int) : TextColor {
    override fun value(): Int = rgbValue
}

object ChatColor {
    val BLACK = Color(0x181425)
    val DARK_BLUE = Color(0x124e89)
    val DARK_GREEN = Color(0x265c42)
    val DARK_AQUA = Color(0x3a4466)
    val DARK_RED = Color(0xa22633)
    val DARK_PURPLE = Color(0x68386c)
    val GOLD = Color(0xfeae34)
    val GRAY = Color(0x8b9bb4)
    val DARK_GRAY = Color(0x5a6988)
    val BLUE = Color(0x0099db)
    val GREEN = Color(0x63c74d)
    val AQUA = Color(0x2ce8f5)
    val RED = Color(0xe43b44)
    val LIGHT_PURPLE = Color(0xb55088)
    val YELLOW = Color(0xfee761)
    val WHITE = Color(0xffffff)
}

object FontKey {
    val DEFAULT = Key.key("default");

    val ICON = Key.key("mcw-icons"); // Used for icons e.g. coins
    val PIXEL = Key.key("mcw-playerheads"); // Used to display individual pixels in chat, e.g. for playerheads
    val FADE = Key.key("mcw-fade"); // Used for the world fade in/out effect
}