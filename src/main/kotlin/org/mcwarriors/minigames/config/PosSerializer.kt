package org.mcwarriors.minigames.config

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.minestom.server.coordinate.Pos as Position

typealias Pos = @Serializable(PosSerializer::class) Position

@Serializable
@SerialName("Pos")
private data class PosSurrogate(val x: Double, val y: Double, val z: Double, val yaw: Float, val pitch: Float)


object PosSerializer : KSerializer<Pos> {
    override val descriptor: SerialDescriptor = PosSurrogate.serializer().descriptor

    override fun serialize(encoder: Encoder, value: Pos) {
        val surrogate = PosSurrogate(value.x, value.y, value.z, value.yaw, value.pitch)
        encoder.encodeSerializableValue(PosSurrogate.serializer(), surrogate)
    }

    override fun deserialize(decoder: Decoder): Pos {
        val surrogate = decoder.decodeSerializableValue(PosSurrogate.serializer())
        return Pos(surrogate.x, surrogate.y, surrogate.z, surrogate.yaw, surrogate.pitch)
    }
}