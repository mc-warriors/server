package org.mcwarriors.minigames.config

import kotlinx.serialization.Serializable
import net.minestom.server.item.Material

@Serializable
@JvmInline
value class PosWrapper(val pos: Pos)

@Serializable
@JvmInline
value class MaterialWrapper(val material: Material)
