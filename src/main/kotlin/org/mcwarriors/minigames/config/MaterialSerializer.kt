package org.mcwarriors.minigames.config

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.minestom.server.item.Material

typealias Material = @Serializable(MaterialSerializer::class) Material

@Serializable
@SerialName("Material")
@JvmInline
private value class MaterialSurrogate(val nsId: String)

object MaterialSerializer : KSerializer<Material> {
    override val descriptor: SerialDescriptor = MaterialSurrogate.serializer().descriptor

    override fun serialize(encoder: Encoder, value: Material) {
        val surrogate = MaterialSurrogate(value.namespace().asString())
        encoder.encodeSerializableValue(MaterialSurrogate.serializer(), surrogate)
    }

    override fun deserialize(decoder: Decoder): Material {
        val surrogate = decoder.decodeSerializableValue(MaterialSurrogate.serializer())
        return Material.fromNamespaceId(surrogate.nsId) ?: throw IllegalArgumentException("invalid material id")
    }
}