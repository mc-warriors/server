package org.mcwarriors.minigames.commands

import net.kyori.adventure.text.Component
import net.kyori.adventure.text.event.ClickEvent
import net.kyori.adventure.text.event.HoverEvent
import net.kyori.adventure.text.format.TextColor
import net.kyori.adventure.text.format.TextDecoration
import net.minestom.server.command.builder.Command
import net.minestom.server.command.builder.arguments.ArgumentType
import org.mcwarriors.minigames.Manager
import org.mcwarriors.minigames.constants.ChatColor
import org.mcwarriors.minigames.database.Database
import org.mcwarriors.minigames.file.ResourceManager
import java.io.File
import java.io.IOException

class DatabaseCommand : Command("database") {

    init {
        setDefaultExecutor { sender, context ->
            sender.sendMessage(Component.text("Usage: /$name <backup/restore/clean> ...", ChatColor.RED))
        }


        val actionArgument = ArgumentType.Word("action").from("backup", "restore", "clean")
        val restoreNameArgument = ArgumentType.String("backupName")
        restoreNameArgument.setDefaultValue("") // makes it optional


        addSyntax({ sender, context ->
            when (context.get(actionArgument).lowercase()) {
                "backup" -> {

                    val backupFileName = Database.backup()
                    if(!backupFileName.isEmpty()) {
                        sender.sendMessage(
                            Component.text("Backup created under alias ")
                                .append(Component.text(backupFileName, ChatColor.GRAY).decorate(TextDecoration.ITALIC))
                        )
                    } else {
                        sender.sendMessage(
                            Component.text("Backup creation failed, check the console for details.", ChatColor.RED)
                        )
                    }

                }

                "restore" -> {

                    if (context.get(restoreNameArgument).isEmpty()) { // just /database restore has been executed

                        // print available database restore points with click handlers
                        sender.sendMessage("Available restore points: ")

                        ResourceManager.DIR_DATABASE.listFiles()?.filter { it.name.endsWith("backup") }
                            ?.forEach {
                                sender.sendMessage(
                                    Component.text(" ➥ ").append(
                                        Component.text(it.name, ChatColor.GRAY))
                                        .clickEvent(ClickEvent.runCommand("/database restore \"${it.name}\""))
                                        .hoverEvent(HoverEvent.showText(Component.text("Click to restore \"${it.name.substring(0, 10)}...\"")))
                                )
                            }


                    } else { // /database restore <backup_name> has been executed

                        if(Database.restore(context.get(restoreNameArgument))) {
                            sender.sendMessage(
                                Component.text("Database restored ")
                                    .append(Component.text(context.get(restoreNameArgument), ChatColor.GRAY)).decorate(TextDecoration.ITALIC))
                        } else {
                            sender.sendMessage(
                                Component.text("An error occurred while restoring the database, check the console for details.", ChatColor.RED)
                            )
                        }

                    }
                }

                "clean" -> {
                    Database.clean(true)
                    sender.sendMessage(Component.text("Database cleaned"))
                }
            }
        }, actionArgument, restoreNameArgument)
    }
}