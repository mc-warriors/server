package org.mcwarriors.minigames.teams

import org.mcwarriors.minigames.constants.ChatColor
import org.mcwarriors.minigames.constants.Color

enum class Team(val id: Long, val teamName: String, val color: Color) {
    RED(0, "Red", ChatColor.RED),
    ORANGE(1, "Orange", ChatColor.GOLD),
    YELLOW(2, "Yellow", ChatColor.YELLOW),
    GREEN(3, "Green", ChatColor.GREEN),
    AQUA(4, "Aqua", ChatColor.AQUA),
    BLUE(5, "Blue", ChatColor.BLUE),
    PURPLE(6, "Purple", ChatColor.DARK_PURPLE),
    PINK(7, "Pink", ChatColor.LIGHT_PURPLE);
}