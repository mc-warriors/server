package org.mcwarriors.minigames

import net.minestom.server.MinecraftServer

object Manager {
    val command = MinecraftServer.getCommandManager()
    val connection = MinecraftServer.getConnectionManager()
    val event = MinecraftServer.getGlobalEventHandler()
    val instance = MinecraftServer.getInstanceManager()
    val scheduler = MinecraftServer.getSchedulerManager()
    val logger = MinecraftServer.LOGGER!!
    val server = MinecraftServer.getServer()!!
    lateinit var minecraftServer: MinecraftServer
}