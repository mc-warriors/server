package org.mcwarriors.minigames.games

import net.minestom.server.entity.Player
import net.minestom.server.event.Event
import net.minestom.server.event.EventNode
import org.mcwarriors.minigames.Manager
import java.util.*

typealias Game = suspend (List<Player>, EventNode<in Event>) -> Unit

suspend fun playGame(players: List<Player>, game: Game) {
    val eventRoot = EventNode.all(UUID.randomUUID().toString())
    Manager.event.addChild(eventRoot)
    game(players, eventRoot)
    Manager.event.removeChild(eventRoot)
}

suspend fun eventScope(eventRoot: EventNode<in Event>, attachListeners: suspend (EventNode<in Event>) -> Unit) {
    val childNode: EventNode<in Event> = EventNode.all(UUID.randomUUID().toString())
    eventRoot.addChild(childNode)
    attachListeners.invoke(childNode)
    eventRoot.removeChild(childNode)
}