package org.mcwarriors.minigames.games

import kotlinx.coroutines.delay
import net.kyori.adventure.sound.Sound
import net.kyori.adventure.text.Component
import net.minestom.server.entity.Player
import net.minestom.server.sound.SoundEvent
import org.mcwarriors.minigames.constants.ChatColor

suspend fun countdown(seconds: Int, players: List<Player>) {
    for(i in seconds downTo 1) {
        if(i <= 5 || i % 5 == 0) { // 1,2,3,4,5 or any multiple of 5
            val message = Component.text("The game starts in ", ChatColor.WHITE)
                .append(Component.text(i, ChatColor.BLUE).append(Component.text(" seconds", ChatColor.WHITE)))

            players.forEach {
                it.sendMessage(message)
                it.playSound(Sound.sound(SoundEvent.UI_BUTTON_CLICK, Sound.Source.PLAYER, 0.8f, if (i <= 5) 1.0f else 1.0f))
            }
        }

        delay(1000)
    }

    players.forEach {
        it.sendMessage(Component.text("Game started!", ChatColor.WHITE))
        it.playSound(Sound.sound(SoundEvent.BLOCK_NOTE_BLOCK_PLING, Sound.Source.PLAYER, 1.0f, 1.4f))
    }
}