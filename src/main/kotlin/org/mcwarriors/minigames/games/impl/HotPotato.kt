package org.mcwarriors.minigames.games.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.withTimeoutOrNull
import net.kyori.adventure.sound.Sound
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.Style
import net.kyori.adventure.text.format.TextDecoration
import net.minestom.server.coordinate.Pos
import net.minestom.server.coordinate.Vec
import net.minestom.server.entity.Entity
import net.minestom.server.entity.EntityType
import net.minestom.server.entity.LivingEntity
import net.minestom.server.entity.Player
import net.minestom.server.entity.PlayerSkin
import net.minestom.server.entity.damage.DamageType
import net.minestom.server.entity.metadata.LivingEntityMeta
import net.minestom.server.event.Event
import net.minestom.server.event.EventNode
import net.minestom.server.event.entity.EntityAttackEvent
import net.minestom.server.event.entity.EntityTickEvent
import net.minestom.server.event.inventory.InventoryPreClickEvent
import net.minestom.server.event.player.PlayerChangeHeldSlotEvent
import net.minestom.server.event.player.PlayerDisconnectEvent
import net.minestom.server.event.player.PlayerMoveEvent
import net.minestom.server.instance.Instance
import net.minestom.server.item.ItemComponent
import net.minestom.server.item.ItemStack
import net.minestom.server.item.Material
import net.minestom.server.item.component.HeadProfile
import net.minestom.server.network.packet.server.play.DamageEventPacket
import net.minestom.server.sound.SoundEvent
import net.minestom.server.tag.Tag
import org.mcwarriors.minigames.constants.ChatColor
import org.mcwarriors.minigames.constants.SkullDatabase
import org.mcwarriors.minigames.dsl.builder
import org.mcwarriors.minigames.dsl.itemName
import org.mcwarriors.minigames.dsl.listen
import org.mcwarriors.minigames.extension.bold
import org.mcwarriors.minigames.extension.italic
import org.mcwarriors.minigames.extension.mutablePartition
import org.mcwarriors.minigames.extension.noItalic
import org.mcwarriors.minigames.games.Game
import org.mcwarriors.minigames.games.countdown
import org.mcwarriors.minigames.games.eventScope
import java.util.*
import kotlin.math.cos
import kotlin.math.sin

// These constants can be defined in a config file
private const val ROUND_LENGTH = 30 // Time of rounds, in seconds
private const val ROUND_DELAY = 10 // Time between rounds, in seconds

private val POTATO_ITEM = ItemStack.builder(Material.BAKED_POTATO)
    .apply { itemName = Component.text("Hot Potato", ChatColor.RED).bold().noItalic() }.build()

fun hasPotato(player: Player) = player.inventory.itemInMainHand == POTATO_ITEM
fun givePotato(player: Player) {
    player.inventory.itemInMainHand = POTATO_ITEM
    player.instance.playSound(
        Sound.sound(SoundEvent.BLOCK_FIRE_EXTINGUISH, Sound.Source.PLAYER, 1.0f, 1.0f),
        player.position
    )
}

fun removePotato(player: Player) {
    player.inventory.itemInMainHand = ItemStack.AIR
}

// TODO return positive if all remaining players are on the same team.
fun winPredicate(players: List<Player>) = players.size <= 1

class HotPotato {
    private lateinit var players: MutableList<Player>
    private var roundCount = 1

    fun run(instance: Instance): Game = { initialPlayers, eventRoot ->
        this.players = initialPlayers.toMutableList()

        // TODO Teleport players to new instance. Preferably wait for this to occur.

        countdown(5, players)

        while (true) {
            eventScope(eventRoot) { round(eventRoot, instance) }

            if (winPredicate(players)) {
                // Winner!

                break
            }

            roundCount++
            eventScope(eventRoot, this::betweenRounds)
        }

        // Win logic
        instance.players.forEach {
            it.sendMessage(Component.text("${players.firstOrNull()?.username} has won the game!", ChatColor.GOLD))
            it.playSound(Sound.sound(SoundEvent.ENTITY_PLAYER_LEVELUP, Sound.Source.PLAYER, 1.0f, 1.0f))
        }
    }

    private suspend fun round(ev: EventNode<in Event>, instance: Instance) {
        val prematureEnd = Channel<Unit>()

        ev.listen<PlayerChangeHeldSlotEvent> { it.isCancelled = true }
        ev.listen<InventoryPreClickEvent> { it.isCancelled = true }
        ev.builder<PlayerDisconnectEvent> {
            asyncHandler { event ->
                val isGamePlayer = players.remove(event.player)
                if (isGamePlayer) {
                    players.forEach {
                        it.sendMessage(Component.text("${event.player.username} disconnected"))
                        it.playSound(Sound.sound(SoundEvent.ENTITY_PLAYER_HURT, Sound.Source.PLAYER, 1.0f, 1.0f))
                    }
                }

                // Need to handle the win within this function
                if (winPredicate(players)) {
                    prematureEnd.send(Unit)
                }
            }
        }
        ev.listen<EntityAttackEvent> {
            if (it.target !is Player && it.entity !is Player) return@listen
            val target = it.target as Player
            val entity = it.entity as Player

            pvpHandler(it)

            if (hasPotato(entity) && !hasPotato(target)) {
                removePotato(entity)
                givePotato(target)
                entity.sendMessage(Component.text("You tagged ${target.username}!", ChatColor.RED))
                target.sendMessage(Component.text("${entity.username} tagged you!", ChatColor.RED))
            }
        }

        players.forEach { pl ->
            pl.inventory.clear()
            pl.setHeldItemSlot(0)
            // pl.showTitle(Title.title(Component.text("ROUND $rounds", ChatColor.WHITE), Component.empty(), Times.times(Duration.ofMillis(500), Duration.ofMillis(2000), Duration.ofMillis(500))))
        }

        val initialPotatoPlayers = players.shuffled().take(players.size / 2)
        initialPotatoPlayers.forEach { pl ->
            givePotato(pl)
            pl.sendMessage(
                Component.text("You have started with the potato!", ChatColor.RED).decorate(TextDecoration.BOLD)
            )
        }

        Powerup(Pos(10.0, 1.5, 0.0), "coins", ev, instance)
        Powerup(Pos(16.0, 1.5, 0.0), "speed", ev, instance)
        Powerup(Pos(4.0, 1.5, 0.0), "slowness", ev, instance)

        withTimeoutOrNull(ROUND_LENGTH * 1000L) {
            prematureEnd.receive()
        }

        val deadPlayers = players.mutablePartition { pl -> pl.itemInMainHand == POTATO_ITEM }
        deadPlayers.forEach { pl ->
            pl.itemInMainHand = ItemStack.AIR
            pl.playSound(Sound.sound(SoundEvent.ENTITY_ENDER_DRAGON_GROWL, Sound.Source.HOSTILE, 1.0f, 1.0f))
            pl.sendMessage(Component.text("You burned to a crisp!", ChatColor.RED).decorate(TextDecoration.BOLD))
            // TODO Make them spectator
        }
    }

    private suspend fun betweenRounds(ev: EventNode<in Event>) {
        ev.listen<PlayerChangeHeldSlotEvent> { it.isCancelled = true }

        players.forEach { pl ->
            pl.inventory.clear()
            pl.setHeldItemSlot(0)
        }
        betweenRoundCountdown(ROUND_DELAY, roundCount, players)
    }
}

// Note this hit delay implementation could have a person "comboed" from two different players, as all players have separate hit cooldowns.
const val HIT_DELAY_TICKS = 10 // 10 ticks = 0.5s
fun pvpHandler(ev: EntityAttackEvent) {
    val tag: Long? = ev.entity.getTag(Tag.Long("last_hit"))
    if (ev.entity.aliveTicks < (tag ?: 0) + HIT_DELAY_TICKS) return

    ev.instance.players.forEach { pl ->
        pl.sendPacket(
            DamageEventPacket(
                ev.target.entityId,
                0,
                ev.entity.entityId + 1,
                ev.entity.entityId,
                ev.entity.position
            )
        )
    }
    ev.target.takeKnockback(
        0.4f,
        sin(Math.toRadians(ev.entity.position.yaw.toDouble())),
        -cos(Math.toRadians(ev.entity.position.yaw.toDouble()))
    )

    ev.entity.setTag(Tag.Long("last_hit"), ev.entity.aliveTicks)
}

suspend fun betweenRoundCountdown(seconds: Int, roundNumber: Int, players: List<Player>) {
    for (i in seconds downTo 1) {
        if (i <= 5 || i % 5 == 0) {
            val message = Component.text("Round $roundNumber starts in ", ChatColor.WHITE)
                .append(Component.text(i, ChatColor.BLUE).append(Component.text(" seconds", ChatColor.WHITE)))

            players.forEach { it.sendMessage(message) }
        }

        delay(1000)
    }

    players.forEach {
        it.sendMessage(Component.text("Round $roundNumber started!", ChatColor.WHITE))
        it.playSound(Sound.sound(SoundEvent.BLOCK_NOTE_BLOCK_PLING, Sound.Source.PLAYER, 1.0f, 1.4f))
    }
}

class Powerup(pos: Pos, type: String, eventNode: EventNode<in Event>, instance: Instance) {
    val powerupEventNode: EventNode<in Event>;

    val skin: HeadProfile
    val type: String
    val nameComponent: Component

    val entity: Entity;
    var position: Pos
    var animationLife = 0

    init {
        this.powerupEventNode = EventNode.all(UUID.randomUUID().toString())
        eventNode.addChild(this.powerupEventNode)

        this.position = pos;
        this.type = type;

        when(type) {
            "speed" -> {
                skin = SkullDatabase.bluePowerup
                nameComponent = Component.text("Speed").color(ChatColor.AQUA).decorate(TextDecoration.BOLD)
            }
            "slowness" -> {
                skin = SkullDatabase.purplePowerup
                nameComponent =Component.text("Slowness").color(ChatColor.DARK_PURPLE).decorate(TextDecoration.BOLD)
            }
            "coins" -> {
                skin = SkullDatabase.orangePowerup
                nameComponent = Component.text("Coins").color(ChatColor.GOLD).decorate(TextDecoration.BOLD)
            }
            else -> {
                TODO("Unimplemented")
            }
        }

        entity = LivingEntity(EntityType.ARMOR_STAND)
        entity.setNoGravity(true)
        entity.isInvisible = true
        entity.isCustomNameVisible = true
        entity.customName = nameComponent
        entity.instance = instance
        entity.teleport(pos)

        var skullItem = ItemStack.builder(Material.PLAYER_HEAD).build()
        skullItem = skullItem.with(ItemComponent.PROFILE, skin)
        entity.helmet = skullItem


        this.powerupEventNode.addListener(PlayerMoveEvent::class.java) {
            if(!it.player.intersectBox(it.player.position.sub(this.entity.position), this.entity.boundingBox.expand(0.6, 0.6, 0.6))) return@addListener

            remove(eventNode)
            it.player.playSound(Sound.sound(SoundEvent.ENTITY_ITEM_PICKUP, Sound.Source.PLAYER, 1.0f, 1.2f))
            it.player.sendMessage(Component.text("You claimed a ").append(nameComponent).append(Component.text(" powerup!").style(Style.empty())))
        }

        this.powerupEventNode.addListener(EntityTickEvent::class.java) {
            animationLife++
            this.position = this.position.add(Vec(0.0, sin((animationLife / 30.0)) * 0.02, 0.0)).withYaw { yaw -> yaw + 2.0 }
            this.entity.teleport(this.position)
        }
    }

    fun remove(eventNode: EventNode<in Event>) {
        this.entity.remove()
        eventNode.removeChild(this.powerupEventNode)
    }
}