package org.mcwarriors.minigames

import com.github.shynixn.mccoroutine.minestom.launch
import net.kyori.adventure.sound.Sound
import net.kyori.adventure.text.Component
import net.minestom.server.MinecraftServer
import net.minestom.server.command.builder.Command
import net.minestom.server.coordinate.Pos
import net.minestom.server.event.player.AsyncPlayerConfigurationEvent
import net.minestom.server.event.player.PlayerSpawnEvent
import net.minestom.server.extras.MojangAuth
import net.minestom.server.instance.LightingChunk
import net.minestom.server.instance.block.Block
import net.minestom.server.sound.SoundEvent
import org.mcwarriors.minigames.commands.DatabaseCommand
import org.mcwarriors.minigames.constants.ChatColor
import org.mcwarriors.minigames.database.Database
import org.mcwarriors.minigames.dsl.listen
import org.mcwarriors.minigames.file.ResourceManager
import org.mcwarriors.minigames.games.impl.HotPotato
import org.mcwarriors.minigames.games.playGame
import org.mcwarriors.minigames.teams.Team

fun main() {
    val server = MinecraftServer.init()
    Manager.minecraftServer = server;
    // MojangAuth.init()

    if (ResourceManager.mkdirDefaults()) {
        Manager.logger.warn("Default directory(s) were made, so not all files and configs may be present for stable functionality.")
    }

    // This instance is purely for testing, remove in favour of proper lobby ASAP
    val defaultInstance = Manager.instance.createInstanceContainer()
    defaultInstance.setChunkSupplier(::LightingChunk)
    defaultInstance.setGenerator { it.modifier().fillHeight(0, 2, Block.POLISHED_DIORITE) }


    Manager.event.listen<AsyncPlayerConfigurationEvent> {
        it.spawningInstance = defaultInstance;
        it.player.respawnPoint = Pos(0.0, 4.0, 0.0)

        // Team will have to be configurable. Red team is just for testing.
        // Note for testing, you will need a UUID handler to ensure players have consistent UUIDs.
        Database.players.insertWithTeam(it.player.uuid.toString().toByteArray(), Team.RED.id)
    }

    Manager.event.listen<PlayerSpawnEvent> {
        it.player.sendMessage(
            Component.text("Welcome to ", ChatColor.WHITE).append(Component.text("MCWarriors", ChatColor.BLUE))
        )
        it.player.playSound(Sound.sound(SoundEvent.ENTITY_PLAYER_LEVELUP, Sound.Source.PLAYER, 0.6f, 1.0f))
    }

    Manager.command.register(Command("hp").apply {
        addSyntax({ sender, ctx ->
            server.launch {
                playGame(
                    Manager.connection.onlinePlayers.toList(),
                    HotPotato().run(defaultInstance) // Instance needs generating
                )
            }
        })
    })

    Manager.command.register(DatabaseCommand())


    server.start("127.0.0.1", 25565)
}