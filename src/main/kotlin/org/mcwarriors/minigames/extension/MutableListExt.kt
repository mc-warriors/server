package org.mcwarriors.minigames.extension

/**
 * Removes all elements matching the [predicate], and returns a new list of elements removed.
 *
 * @param predicate the predicate for item removal
 * @return the list of elements removed
 */
fun <T> MutableList<T>.mutablePartition(predicate: (T) -> Boolean): List<T> {
    val removedItems = mutableListOf<T>()
    val listIterator = this.listIterator()

        while (listIterator.hasNext()) {
            val item = listIterator.next()
            if (predicate(item)) {
                removedItems.add(item)
                listIterator.remove()
            }
        }

    return removedItems
}