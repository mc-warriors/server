/*
Forked from KotStom https://github.com/bladehuntmc/KotStom

Original license notice:
MIT License

Copyright (c) 2020 LeoDog896
Copyright (c) 2024 BladehuntMC
Copyright (c) 2024 oglassdev

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
*/

package org.mcwarriors.minigames.extension

import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.TextColor
import net.kyori.adventure.text.format.TextDecoration
import net.kyori.adventure.text.minimessage.MiniMessage
import net.kyori.adventure.text.minimessage.tag.resolver.TagResolver
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer

/**
 * Converts a `String` to a `Component` using a MiniMessage `TagResolver`. If no `TagResolver` is
 * specified, then it uses the standard `TagResolver`.
 *
 * @param tagResolver The TagResolver to use
 * @return A formatted `Component`
 * @author oglassdev
 */
fun String.asMini(tagResolver: TagResolver = TagResolver.standard()): Component =
    MiniMessage.miniMessage().deserialize(this, tagResolver)

/**
 * Converts a `String` to a `Component` using the `LegacyComponentSerializer`. Prefer using
 * MiniMessage when possible.
 *
 * @return A formatted `Component`
 * @author oglassdev
 */
fun String.asLegacyAmpersand(): Component =
    LegacyComponentSerializer.legacyAmpersand().deserialize(this)

/**
 * Converts a `String` to a `Component` with a color
 *
 * @param color The color to apply
 * @return A `Component`
 * @author oglassdev
 */
fun String.color(color: TextColor?): Component = Component.text(this, color)

/**
 * Converts a `String` to a `Component`
 *
 * @return A `Component`
 * @author oglassdev
 */
fun String.asComponent(): Component = Component.text(this)

/**
 * Converts a `String` to a `Component` with a decoration
 *
 * @param color The color to apply
 * @return A `Component`
 * @author oglassdev
 */
fun String.decorate(decoration: TextDecoration, value: Boolean = true): Component =
    Component.text(this).decoration(decoration, value)