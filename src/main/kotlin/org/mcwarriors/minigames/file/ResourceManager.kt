package org.mcwarriors.minigames.file

import java.io.File

/**
 * General manager and helper object for the data and files of the MCWarriors event.
 */

object ResourceManager {
    val DIR_ROOT = File("./data");
    val DIR_DATABASE = File("$DIR_ROOT/database")
    val DIR_CONFIG = File("$DIR_ROOT/config")
    val DIR_WORLD = File("$DIR_ROOT/world")
    val DIR_RESOURCE_PACK = File("$DIR_ROOT/resource_pack")

    /**
     * Makes the default directories for runtime.
     *
     * @return true if any directories were made.
     */
    fun mkdirDefaults(): Boolean {
        return DIR_ROOT.mkdir()
                || DIR_DATABASE.mkdir()
                || DIR_CONFIG.mkdir()
                || DIR_WORLD.mkdir()
                || DIR_RESOURCE_PACK.mkdir()
    }
}

