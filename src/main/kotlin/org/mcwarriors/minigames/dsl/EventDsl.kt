/*
Forked from KotStom https://github.com/bladehuntmc/KotStom

Original license notice:
MIT License

Copyright (c) 2020 LeoDog896
Copyright (c) 2024 BladehuntMC
Copyright (c) 2024 oglassdev

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
*/

package org.mcwarriors.minigames.dsl

import com.github.shynixn.mccoroutine.minestom.addSuspendingListener
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import net.minestom.server.event.Event
import net.minestom.server.event.EventListener
import net.minestom.server.event.EventNode
import org.mcwarriors.minigames.Manager

@DslMarker @Target(AnnotationTarget.FUNCTION, AnnotationTarget.TYPE) annotation class EventDSL

/**
 * An `EventListener` builder
 *
 * @author oglassdev
 */
data class EventBuilder<T : Event>(val clazz: Class<T>) : EventListener.Builder<T>(clazz) {
    /**
     * The amount of times to run until expiration of the `EventListener`
     *
     * @author oglassdev
     */
    var expireCount: Int
        get() =
            throw IllegalAccessError("Expire count can't be accessed in the context of a builder.")
        set(value) {
            expireCount(value)
        }

    /**
     * A coroutine handler that blocks until completion
     *
     * @param block The suspending code to run
     * @author oglassdev
     */
    inline fun blockingHandler(crossinline block: suspend (T) -> Unit) = handler {
        runBlocking { block(it) }
    }

    /**
     * A coroutine handler that runs asynchronously using the `MinestomDispatcher`
     *
     * @param context The `CoroutineContext` to launch the coroutine in
     * @param block The suspending code to run
     * @author oglassdev
     */
    inline fun asyncHandler(
        context: CoroutineContext = Dispatchers.Default, // Modified by @Mqlvin to remove unnecessary `MinestomDispatcher.kt` file
        crossinline block: suspend (T) -> Unit
    ) = handler { CoroutineScope(context).launch { block(it) } }
}

/**
 * A builder DSL for `EventListener`
 *
 * @param E The `Event` to listen for
 * @author oglassdev
 */
inline fun <reified E : Event> EventNode<in E>.builder(
    block: @EventDSL EventBuilder<E>.() -> Unit
): EventNode<in E> = addListener(EventBuilder(E::class.java).apply(block).build())

/**
 * Listens for an event
 *
 * @param E The `Event` to listen for
 * @param block The listener
 * @author oglassdev
 */
inline fun <reified E : Event> EventNode<in E>.listen(
    noinline block: (E) -> Unit
): EventNode<in E> = addListener(E::class.java, block)